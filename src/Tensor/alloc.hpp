#ifndef _ALLOC_HPP
#define _ALLOC_HPP

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <alloca.h>

// Note: benefits from being tuned for specific architecture. Could do this with #ifdef...s for AVX512, etc.
const unsigned long int ALLOCATION_ALIGNMENT = 512;

// TODO: write aligned_realloc, which postprocesses with posix_memalign.

template <typename T>
T* aligned_malloc(unsigned long num_elements) {
  T*result = (T*)aligned_alloc(ALLOCATION_ALIGNMENT, num_elements*sizeof(T));
  //  T*result = (T*)aligned_alloc(ALLOCATION_ALIGNMENT, num_elements*sizeof(T));
  assert(result != NULL);
  return result;
}

template <typename T>
T* aligned_calloc(unsigned long num_elements) {
  T*result = aligned_malloc<T>(num_elements);
  assert(result != NULL);
  memset(result, 0, sizeof(T)*num_elements);
  return result;
}

// Variable length array stack allocation:
template <typename T>
T* vla_alloc(unsigned long num_elements) {
  return (T*)alloca(num_elements*sizeof(T));
}

#endif
